# IC-INTERN2023

本仓库由摸鱼范式维护，欢迎关注摸鱼范式公众号。本仓库收集了2023届的IC相关实习信息，如果你也想提供实习信息，可以通过摸鱼范式公众号后台提供，或者在本仓库的issues中提出。`建议有需要的同学收藏本仓库的网址`

# 交流区

## 2023交流群

摸鱼范式-2023届数字IC交流群，628200294

请随机添加三位的微信之一，他们会拉你进微信群，加好友请备注`自己要进2023校招微信交流群`+`学校`+`年级`

![](doc/wxgroup.png)

给2023届的学生提供一个学习交流，实习分享，备战秋招的平台，如果你有看到实习机会，欢迎大家分享到群里

目前审批只允许2023以及往后年份毕业的学生加入，群容量有限，请见谅

## 职场交流群

摸鱼范式-芯片技术与职场交流群，754075465

## 公众号

欢迎关注公众号`摸鱼范式`

![](README.assets/qrcode.png)

# 广告区

### 验证培训

一年只有两次的路科验证V2pro已经开始了，扫描二维码联系路科验证MOMO，然后报出暗号`摸鱼范式`就能获得`200优惠券`！对于课程学习有任何问题，都可以在`摸鱼范式`公众号后台联系我。

👇课程详情👇

[21V2Pro秋季班开始售票，我们在膨胀的IC业当下依然只开两期班](http://mp.weixin.qq.com/s?__biz=MzIyNjQ0OTcyOA==&mid=2247493253&idx=1&sn=f621d59fe9906b9d62875080ab536a41&chksm=e872e6aadf056fbc2b4a242d7b68db6ae811ec3a643fd6e70f7acfa02128c74dbc32279d90f8&scene=21#wechat_redirect)

![](README.assets/imgimage-20211022220956017.png)

### 设计培训

对`设计`更加感兴趣的同学可以关注由`15年前端经验的工程师SKY`带来的`数字IC设计从入门到精通课程`，详情请点击下面的链接了解

👉[数字IC/FPGA设计入门课程](https://mp.weixin.qq.com/s?__biz=MzA3NzUzNTk5Ng==&mid=2247496158&idx=1&sn=97f1f0307984231d21e9df9d0a58d911&chksm=9f523e4da825b75b162775996502ec3332fdf533a5d11113357a596db7ee040423923f1dec74&token=2123398371&lang=zh_CN#rd)👈

https://ke.qq.com/course/3133628?tuin=64ce5e2a

# ZEKU

zeku招实习生，上海验证岗，要求实习时间6个月以上，硕士985/211，本科不要求，专业电子相关，研一/二/三(没投过zeku校招的)都可以，有意向的可以简历直接发邮箱：nslwei@qq.com

# AMD

详情见[PDF文件](./doc/AMD在招实习岗位(2021年10月).pdf)

# ARM CHINA

JD见[ARM CHINA](./doc/ARM_CHINA.md)

内推链接

https://app.mokahr.com/m/recommendation-apply/armchina/886?sharePageId=714637&recommendCode=AGRBw&codeType=1&hash=%23%2Frecommendation%2Fpage%2F714637  

点击校园招聘内推，岗位列表末尾有实习生岗位

推荐人填写`Maoquan Zhang`

# NV

JD见[NV](./doc/英伟达2023实习生招聘.pdf)

投递方式：发简历到邮箱bellaw@nvidia.com

# 商汤

详情见[商汤实习](./doc/商汤实习.md)

# 奥比中光

详情见[奥比中光](./doc/奥比中光.md)

# 计算所

详情见[中科院计算所](./doc/中科院计算所.md)

# 乐鑫

详情见[领跑者计划 I 乐鑫科技 2023 届实习生招募正式启动！](https://mp.weixin.qq.com/s/3TYMTr1AgHy22duC3v7RqQ)

内推码

![](./doc/乐鑫.jpg)

# 昆仑芯(百度)

## 内推
`姓名+岗位+地点`为标题，将简历通过邮箱发送至`shijinxiang@baidu.com`

## JD

详情见官网

https://talent.baidu.com/external/baidu/index.html#/intern/INTERN/%E6%98%86%E4%BB%91

# 字节跳动

详情见[字节跳动](./doc/字节跳动.md)

# 地平线
详情见[地平线](./doc/地平线.md)

# optiver

[【FPGA实习】](https://mp.weixin.qq.com/s/MoOcpbUWIzDt5HDNzrii6Q)

![](doc/optiver.png)

# 高拓讯达

[【高拓讯达】](https://mp.weixin.qq.com/s/vexxvPGtIrlVlcCN-Zxslw
)

# 平头哥
详情见[平头哥](./doc/平头哥.md)

# ASR

翱捷科技股份有限公司，成都分公司

ASIC 设计、验证岗位，招收2023届实习生，限211/985硕士，专业是通信，电工，微电子，自动化的最好。有意向投邮箱`1447373058@qq.com`

# 海思

详情见[海思](./doc/海思.md)

# 禾赛

https://mp.weixin.qq.com/s/CEt7Cg81ILxO0gTS83B5ew

# 联发科
https://mp.weixin.qq.com/s/FWzdqLErcbMgxZgDdS76Gw
![](doc/%E8%81%94%E5%8F%91%E7%A7%91.jpg)

# 中科院计算所-香山处理器团队

实习内容：
- 高性能处理器架构探索：如何设计SPEC 15分/GHz的处理器？
- 芯片敏捷开发基础设施：更高效的功能验证、性能验证等

为什么来这里实习：
- 提供国内顶尖的能力培养环境
- 实习工作条件较好（图为我的工位）
- 实习待遇向互联网公司看齐

![](./doc/香山.jpg)

技能和背景要求：
- 计算机背景的，要求学习过OS、组成原理或体系结构，最好做过相关实验
- 电子IC背景的，要求学习过计算机组成原理或体系结构

实习时间要求：
- 有计算机体系结构研究经历或基于Chisel的处理器开发经验的同学至少3个月
- 无以上经历的同学，我们也可以培养，但是需要实习半年以上

欢迎符合要求的同学与我们联系投递简历，联系方式如下：

邮箱：`wangkaifan@ict.ac.cn`或 `archshinez@outlook.com`

# NV暑期实习

详情见[NV](./doc/nv.md)

# 嘉楠科技
![](./doc/嘉楠科技.png)

# 高通

内推方式：

`hins@qti.qualcomm.com`

注明投递岗位

岗位列表：

http://campus.51job.com/QC2022intern/index04.html

# 美满科技

> 内推请投递邮箱`jiaxinglpro@gmail.com`

https://mp.weixin.qq.com/s/7kymKMXJEwsfd4JS2iH5yQ 

# 飞腾科技
投递方式请看链接

https://mp.weixin.qq.com/s/i7QKbe2YHv-eJ4y6a7PbtQ

# ARM CHINA 暑期实习

内推码，推荐人`Charles Ma`

![](./doc/2022-05-06-00-03-32.png)

# 硅谷数模

https://mp.weixin.qq.com/s/gUXk4RrpYV14M5VYGxBsiA

内推方式

简历发至邮箱`jkwang@ANALOGIXSEMI.COM`



# Lattice Semiconductor 

低功耗、可编程器件的领先供应商-莱迪思半导体实习生招聘

## 【招聘岗位】

FPGA算法、FPGA硬件、FPGA验证、软件验证、FPGA应用工程师

## 【实习待遇】

￥350/day

商业险


免费健身房、免费咖啡等

不定期团建

https://www.shixiseng.com/com/com_1g7ngfkri9yo

详情见[Lattice](./doc/Lattice.pdf)