# Intern - Software Engineer (GPU) 
GS 上海闵行区
## 工作内容：
- 性能相关数据处理
- 性能相关工具开发

## 要求：
- 精通python以及相关库，如JSON，Pandas，Matplotlib等
- 熟悉UNIX环境，了解Arm和x86体系架构
- 熟悉C或者C++
- 有使用软件性能工具的经验优先
- 了解Linux系统以及内核工作原理的优先
- 每周至少到岗3天
- 专业不限，年级不限

# Intern - Hardware Applications Engineer (Performance Model)
GS 上海市闵行区
## Background

Arm’s Partner Enablement Group (PEG) is responsible for providing high quality technical support to customers using Arm’s products. In order to do this, we have a number of Applications Engineering teams based at various locations around the world.

We are now looking for a highly enthusiastic university student with a passion for system architecture exploring on performance model. As an Applications Engineering Intern, you will
join a team of Applications Engineers based in our Shanghai office. You will be working on the performance model of Arm’s system IPs. You will help Arm maintain and strengthen its leadership in the infrastructure world. If you are up to the task, we want to hear from you!


## Job Description

The goal of this project is developing a showcase of Arm’s performance model and technologies targeting on Infrastructure markets. You might be involved in many fields across:
- Demo applications for Arm’s system IP performance model
- Explore the system architecture of high-performance computing
- Analyze application bandwidth at the system level  
- Create the documentation to help customers ramp up the performance model


If you’re passionate about:
- Pursuing the challenges of early architecture exploration in a large Server/HPC SoC
- How to build, test and debug the system traffic on the performance model
- Gaining a deeper understanding of high-level abstraction for a SoC
Then this is a phenomenal opportunity for you!!!


## Qualifications
- Good university degree, in Electronics Engineering or Computer Science ideally, although other science graduates would be considered if they have relevant experience.
- Good knowledge of low-level C or C++  
- Good knowledge of VHDL, Verilog or System Verilog
- Understanding one or more of the following technical areas: computer architecture, bus system (AMBA), etc.


## Desirable Skills and Qualities 
- If you have an interest in any of the following areas, it would improve your application:
- Experience or knowledge of ASIC or FPGA design, simulation and/or verification techniques, including RTL coding and simulation, in Verilog or VHDL
- Exposure to verification techniques and testbenches

## Personal Requirements
- A real passion for SoC architecture
- A creative and structured approach to problem-solving
- A strong will and eagerness for learning and self-improvement
- Be able to schedule own workload and plan tasks
- Fluent in English, with clear and effective written and verbal communication skills.
- Must have good inter-personal skills and be able to work well in a team especially when under pressure
- Must be willing to be flexible and accept new challenges

# Intern - AI Toolchain
GS 上海市闵行区 北京市海淀区 广东深圳市
Job Description:
This job will mainly focus on the following areas, but not limited to:
- Participate in AI toolchain development
- Participate in AI IDE tool development
- Participate in AI toolchain and IDE test 

## Requirements:
- Major in EE, Computer science or related background
- The programming skills in C/C++/Python/Java/JavaScrip
- Familiar with the development tools such as Linux, Git, Makefile and sh
- Fast learning skills

## Personal Skills:
We are proud to have a set of behaviors that reflect our unique culture and guide our decisions, defining how we work together
- Positive, “can do” attitude.
- Self-motivated, dependable, and prompt.
- Flexible, willing to adapt to changing responsibilities and assignments.
- Good communicator, personable.
- Ethical, honest, and trustworthy.

# Software Engineering Intern - Arm Server & GPU
GS 上海市闵行区
## 岗位职责：
- 协助搭建开发环境、验证不同Demo方案
- 根据要求调研、实现、优化指定的Arm服务器与GPU相关技术Demo功能（Linux/Android）
- 调研收集服务器/边缘方向技术细节

## 岗位要求：
- 全日制统招本科以上学历，计算机科学与技术，软件工程，电子信息，通信及其相关专业在读
- 熟悉C/C++/Go等语言，较好的开发基础
- 了解Linux使用，最好有一定开发经验
- 乐于沟通、自我驱动

加分项：
- 熟悉Java/Python/JS等语言
- 有Android系统了解或者开发经验
- 有docker/k8s等容器、云服务技术经验或兴趣
- Nvidia CUDA经验

收获：
- 快速、完整的功能验证、开发经历、相关工具使用
- 前沿技术视野
- 一线公司的实习经历，有助职业规划